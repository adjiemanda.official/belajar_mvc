const { user } = require('../models');
const { Op } = require("sequelize");

exports.getUsers = async (req,res) => {
    try {
        const data = await user.findAll({order: [['id', 'ASC']]});
            res.status(200).json({
                status: true,
                message: `users retrieved!`,
                data
            })
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.getUserByID = async (req, res) => {
    try {
        const data = await user.findByPk(req.params.id);
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `users with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            res.status(200).json({
                status: true,
                message: `users with ID ${req.params.id} retrieved!`,
                data
            })
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.insertUser = async (req, res) => {
    try {
        const [data, created] = await user.findOrCreate({
            where: {email: req.body.email}, 
            defaults: {
                name: req.body.name,
                email: req.body.email,
                password: (req.body.password == null || req.body.password == "") ?'tsel1234' : req.body.password                    
            }
        });
        if (created) {
            res.status(201).json({
                status: true,
                message: 'user created!',
                data
            })
        }
        else {
            res.status(202).json({
                status: false,
                message: 'email already exist!'
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.updateUser = async (req, res) => {
    try {
        const data = await user.findByPk(req.params.id);
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `users with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            try {
                const findEmailUser = await user.findOne({ 
                    where: { 
                      email: req.body.email, 
                      id: {
                        [Op.ne]: req.params.id
                      }
                    }
                });
                if (findEmailUser != null) {
                    res.status(201).json({
                        status: false,
                        message: 'Can not update users because email already exist in other user!',
                    })
                } 
                else {
                    try {
                        let dataupdate = await user.update({
                            name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
                            email: (req.body.email == null || req.body.email == "") ? data.email : req.body.email,
                            password: (req.body.password == null || req.body.password == "") ? data.password : req.body.password
                        }, {where: {id: req.params.id}}
                        )
                        try {
                            const datanew = await user.findByPk(req.params.id);
                            if (dataupdate == 1 && datanew ) {
                                res.status(200).json({
                                    status: true,
                                    message: `${dataupdate} rows executed!!  users with ID ${req.params.id} updated!`,
                                    data,
                                    datanew
                                })
                            }
                        }
                        catch(err) {
                            res.status(422).json({
                                status: false,
                                message: err.message
                            })
                        }
                    }
                    catch(err) {
                        res.status(422).json({
                            status: false,
                            message: err.message
                        })
                    }
                }
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: err.message
                })
            }
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.deleteUser = async (req, res) => {
    try { 
        let n = await user.destroy({where: {id: req.params.id}});
        if (n == 0) {
            res.status(202).json({
                status: false,
                message: `user with id ${req.params.id} not found!`
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: `${n} rows executed!! user with id ${req.params.id} deleted!`
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}