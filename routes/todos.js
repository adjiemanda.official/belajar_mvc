var express = require('express');
var router = express.Router();
const todoControllers = require('../controllers/todoControllers');

/* GET todos listing. */
router.get('/', todoControllers.getTodos);


/* GET todo listing by ID */
router.get('/:id', todoControllers.getTodoByID);


/* POST todo inserting. */
router.post('/', todoControllers.insertTodo);


/* PUT todo updating. */
router.put('/:id', todoControllers.updateTodo);


/* DELETE todo deleting. */
router.delete('/:id', todoControllers.deleteTodo);

module.exports = router;
