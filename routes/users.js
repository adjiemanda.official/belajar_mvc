var express = require('express');
var router = express.Router();
const userControllers = require('../controllers/userControllers');


/* GET users listing. */
router.get('/', userControllers.getUsers);


/* GET users listing by ID */
router.get('/:id', userControllers.getUserByID);


/* POST user inserting. */
router.post('/', userControllers.insertUser);


/* PUT user updating. */
router.put('/:id', userControllers.updateUser);


/* DELETE user deleting. */
router.delete('/:id', userControllers.deleteUser);

module.exports = router;
